using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace task1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.Load += Form1_Load; // Додайте обробник події Load
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Додавання стовпців
            dataGridView1.Columns.Add("Column1", "Column 1");

            // Додавання рядків
            
           for (int i = 0; i < 10; i++)
            {
                dataGridView1.Rows.Add(i);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Отримання масиву з DataGridView
            double[] arr = new double[dataGridView1.Rows.Count];
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                arr[i] = Convert.ToDouble(dataGridView1.Rows[i].Cells[0].Value);
            }

            // Заміна елементів, що розташовані симетрично
            for (int i = 0; i < arr.Length / 2; i++)
            {
                double temp = arr[i];
                arr[i] = arr[arr.Length - i - 1];
                arr[arr.Length - i - 1] = temp;
            }

            // Очищення DataGridView та виведення результату
            dataGridView1.Rows.Clear();
            foreach (double element in arr)
            {
                dataGridView1.Rows.Add(element);
            }
        }
    }
}
