﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab22_task2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int[,] matrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; // приклад матриці
            int max = matrix[0, 0]; // початкове значення максимального елементу
            int row = 0; // початкове значення номера рядка з максимальним елементом
            int product = 1; // початкове значення добутку елементів рядка з максимальним елементом

            // знаходимо максимальний елемент головної діагоналі та його номер рядка
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                if (matrix[i, i] > max)
                {
                    max = matrix[i, i];
                    row = i;
                }
            }

            // знаходимо добуток елементів рядка з максимальним елементом
            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                product *= matrix[row, j];
            }

            MessageBox.Show("Максимальний елемент головної діагоналі: " + max + "\nДобуток елементів рядка з максимальним елементом: " + product);
        }
    }
}

